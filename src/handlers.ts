import path from "node:path";
import type { LambdaFunctionURLHandler } from "aws-lambda";

const BRIDGY_URL = "https://fed.brid.gy/";
const BRIDGY_WEBMENTION_URL = path.join(BRIDGY_URL, "webmention");

export const handleWebhook: LambdaFunctionURLHandler = async (event) => {
  try {
    const body = JSON.parse(event.body ?? "");

    const postUrl: string = body?.post?.current?.url ?? "";

    if (postUrl && body?.post?.current?.status === "published") {
      const res = await fetch(BRIDGY_WEBMENTION_URL, {
        method: "post",
        body: `source=${encodeURIComponent(postUrl)}&target=${encodeURIComponent(
          BRIDGY_URL,
        )}`,
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
      });
      if (res.ok) {
        return { statusCode: 200 };
      }
      throw new Error(res.statusText);
    }
    if (body?.post?.current?.status) {
      console.warn("Post status is not 'published'");
      return { statusCode: 200 };
    }
    throw new Error("Could not locate post url");
  } catch (e) {
    console.error(e);
    return { statusCode: 500 };
  }
};
